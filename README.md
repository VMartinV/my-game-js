# 					Mon Jeu JavaScript



## Introduction

Durée du projet : 3 semaines

L'objectif de ce projet est de faire un petit jeu en Javascript sans framework. Il va permettre d'utiliser le JS, l'algorithmie de base, la manipulation du DOM et la POO dans un contexte ludique... mais professionnel.

Le type et le thème du jeu sont libres (en sachant qu'il sera sur votre portfolio, donc pas n'importe quoi non plus).



##  Organisation

- Choisir le thème et le type du jeu

- Faire 2-3 maquettes fonctionnelles de votre jeu

- Créer un board dans le dépôt gitlab du jeu dans lequel vous listerez toutes les fonctionnalités à coder

- Créer une milestone (un Sprint) par semaine et y indiquer l'état dans lequel vous souhaitez que le jeu soit au bout de ce sprint, assigner les fonctionnalités du board à la milestone

  

##  Aspects techniques obligatoires

- Utilisation des objets Javascript d'une manière ou d'une autre

- Avoir des données dans le JS qui seront modifiées en cours de jeu (barre de vie, score, progression, etc.)

  

## Introduction

Mon jeu Js est inspiré de pokémon mais les personnages dont vous incarner est un personnage isu d'un autre jeu qui c'est retrouver ici à cause d'un bug mystérieux.
Dans ses jeux vos devrais vous déplaçait dans une map jusqu'à rejoindre un personnage qui vous de défiras dans combat pokémon quill faudra gagner.


## Maquettes 



![](img/maquet.png)

![](img/maquet1.png)











