class Point {
	
	constructor(x,y){
		this.x = x;
		this.y = y;
	}
}

class Obstacle {
		
	constructor(x, y, largeur, hauteur, couleur){
		this.x = x;
		this.y = y;
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.couleur = couleur;
		
		var body = document.getElementsByTagName("body")[0];
		var div = document.createElement("div");
		div.setAttribute("class","obstacle");
		body.appendChild(div);
		div.style.left = x+"px";
		div.style.top = y+"px";
		div.style.width = largeur+"px";
		div.style.height = hauteur+"px";
		div.style.backgroundColor=couleur;
		div.style.position = "absolute";
		
		this.div = div;
	}
	
	collision(ptx,pty,width,height){
		var pt1 = new Point(ptx,pty);
		var pt2 = new Point(ptx+width, pty);
		var pt3 = new Point(ptx, pty+height);
		var pt4 = new Point(ptx+width, pty+height);
		return 	   this.contient(pt1) 
				|| this.contient(pt2)
				|| this.contient(pt3)
				|| this.contient(pt4);
	}
	
	contient(pt){
		return ( pt.x>=this.x && pt.x<=(this.x+this.largeur) ) 
		&& ( pt.y>=this.y && pt.y<=(this.y+this.hauteur) ) 
		
	}
	
}