class Hero {

	constructor(type) {
		this.type = "type" + type;

		var body = document.getElementsByTagName("body")[0];
		var div = document.createElement("div");
		div.setAttribute("class", "character " + this.type);
		body.appendChild(div);
		div.style.left = "1465px";
		div.style.top = "830px";
		this.div = div;
	}

	stop() {
		this.div.setAttribute("class", "character " + this.type);
	}



	canMoveLeft(obstacles) {
		var rect = this.div.getBoundingClientRect();
		var newHauteur = rect.height / 3;

		for (var i in obstacles) {
			var obstacle = obstacles[i];
			if (obstacle.collision(rect.x - 4, rect.y + (newHauteur * 2), rect.width, newHauteur))
				return false;
		}

		return true;
	}

	canMoveTop(obstacles) {
		var rect = this.div.getBoundingClientRect();
		var newHauteur = rect.height / 3;

		for (var i in obstacles) {
			var obstacle = obstacles[i];
			if (obstacle.collision(rect.x, rect.y - 4 + (newHauteur * 2), rect.width, newHauteur))
				return false;
		}

		return true;

	}

	canMoveDown(obstacles) {
		var rect = this.div.getBoundingClientRect();
		var newHauteur = rect.height / 3;

		for (var i in obstacles) {
			var obstacle = obstacles[i];
			if (obstacle.collision(rect.x, rect.y + 4 + (newHauteur * 2), rect.width, newHauteur))
				return false;
		}

		return true;
	}

	canMoveRight(obstacles) {
		var rect = this.div.getBoundingClientRect();
		var newHauteur = rect.height / 3;

		for (var i in obstacles) {
			var obstacle = obstacles[i];
			if (obstacle.collision(rect.x + 4, rect.y + (newHauteur * 2), rect.width, newHauteur))
				return false;
		}

		return true;
	}

	moveRight() {
		var rect = this.div.getBoundingClientRect();
		this.div.setAttribute("class", "character moveright " + this.type);
		this.div.style.left = (rect.x + 4) + "px";
	}

	moveTop() {
		var rect = this.div.getBoundingClientRect();
		this.div.setAttribute("class", "character moveup " + this.type);
		this.div.style.top = (rect.y - 4) + "px";
	}

	moveDown() {
		var rect = this.div.getBoundingClientRect();
		this.div.setAttribute("class", "character movedown " + this.type);
		this.div.style.top = (rect.y + 4) + "px";
	}
	moveLeft() {
		var rect = this.div.getBoundingClientRect();
		this.div.setAttribute("class", "character moveleft " + this.type);
		this.div.style.left = (rect.x - 4) + "px";
	}
}