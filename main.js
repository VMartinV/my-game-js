$(document).ready(function() {
  //initialize all variables;
  var damagedone;
  var comment;
  var movecount;
  var attack1 = $('#attack0');
  var attack2 = $('#attack1');
  var attack3 = $('#attack2');
  var attack4 = $('#attack3');
  var attacks = $('.attacklist');
  var options = $('.option');
  var commentBox = $('.commentbox');
  var selfhp = $('#selfhp');
  var opponenthp = $('#opponenthp');
  var selfhpnum = $('#selfcurrenthp');
  var opponenthpnum = $('#opponentcurrenthp');
  var mylastmoveused = undefined;
  var enemylastmoveused = undefined;
  var meneedtorecharge = false;
  var enemyneedtorecharge = false;
  var finished = false;
  var messagewritten = false;
  //pokemon constructor class
  class Pokemon {
    constructor(name, moves, hp, cry, weakness, resistant, frontimage, backimage) {
      this.name = name;
      this.moves = moves;
      this.hp = hp;
      this.cry = cry;
      this.weakness = weakness;
      this.resistant = resistant;
      this.frontimage = frontimage;
      this.backimage = backimage;
    }
    //to show the available attacks
    showoptions() {
      for(var i = 0; i < 4; i++) {
        $("#attack" + i).text(this.moves[i].name);
      }
    }
    //for attacking
    //we need the target  and the move we want to use
    aiattack(opponent, movecount) {
      //take the movecount and store the move in movetouse
      var movetouse = this.moves[movecount];
      //original dmg of move
      var originaldmg = movetouse.dmg;
      //for decidng if the move is going to be a miss
      var unluckynumber = randomgen(1, 11);
      if(unluckynumber == 10) {
        var miss = true;
      } else {
        var miss = false;
      }
      //to prevent move use
      if(enemylastmoveused == solarbeam || enemylastmoveused == hyperbeam) {
        enemyneedtorecharge = true;
      }
      //for taking type advantages
      //initialy set to false everytime attack is called
      var typeadvantaged = false;
      //check if movetouse is target's weakness. if it is we get typeadvantage
      for(var i = 0; i < opponent.weakness.length; i++) {
        if(movetouse.tipe == opponent.weakness[i]) {
          typeadvantaged = true;
        }
      }
      //if we get typeadvantage than our moves damage will multiply by 2
      if(typeadvantaged) {
        movetouse.dmg *= 2;
      }
      var resisted = false;
      for(var i = 0; i < opponent.resistant.length; i++) {
        if(movetouse.tipe == opponent.resistant[i]) {
          resisted = true;
        }
      }


      if(resisted) {
        movetouse.dmg = Math.floor(movetouse.dmg / 2);
      }
      if(!miss && !enemyneedtorecharge) {
        //now reduce the target hp by the movetouse damage
        opponent.hp = opponent.hp - movetouse.dmg;
        //for accurate commentry we need to store how much damage we done in damagedone
        damagedone = movetouse.dmg;
      } else {
        if(enemyneedtorecharge) {
          damagedone = -5;
          enemyneedtorecharge = false;
        } else {
          damagedone = 0;
        }
      }
      //after storing the damage we want to reset back our movetousedamage
      movetouse.dmg = originaldmg;
      //store the move used
      enemylastmoveused = movetouse;
    }
    attack(opponent, movecount) {
      //take the movecount and store the move in movetouse
      var movetouse = this.moves[movecount];
      //original dmg of move
      var originaldmg = movetouse.dmg;
      //for decidng if the move is going to be a miss
      var unluckynumber = randomgen(1, 11);
      if(unluckynumber == 10) {
        var miss = true;
      } else {
        var miss = false;
      }
      //to prevent move use
      if(mylastmoveused == solarbeam || mylastmoveused == hyperbeam) {
        var meneedtorecharge = true;
      }
      //for taking type advantages
      //initialy set to false everytime attack is called
      var typeadvantaged = false;
      //check if movetouse is target's weakness. if it is we get typeadvantage
      for(var i = 0; i < opponent.weakness.length; i++) {
        if(movetouse.tipe == opponent.weakness[i]) {
          typeadvantaged = true;
        }
      }
      //if we get typeadvantage than our moves damage will multiply by 2
      if(typeadvantaged) {
        movetouse.dmg *= 2;
      }
      var resisted = false;
      for(var i = 0; i < opponent.resistant.length; i++) {
        if(movetouse.tipe == opponent.resistant[i]) {
          resisted = true;
        }
      }
      if(resisted) {
        movetouse.dmg = Math.floor(movetouse.dmg / 2);
      }
      if(!miss && !meneedtorecharge) {
        //now reduce the target hp by the movetouse damage
        opponent.hp = opponent.hp - movetouse.dmg;
        //for accurate commentry we need to store how much damage we done in damagedone
        damagedone = movetouse.dmg;
      } else {
        if(meneedtorecharge) {
          damagedone = -5
          meneedtorecharge = false;
        } else {
          damagedone = 0;
        }
      }
      //after storing the damage we want to reset back our movetousedamage
      movetouse.dmg = originaldmg;
      //store the move used
      mylastmoveused = movetouse;
    }
  }
  //Pokemons will use some moves and each move has its own properties
  class Moves {
    constructor(name, dmg, tipe) {
      this.name = name;
      this.dmg = dmg;
      //tipe is actually type of move
      this.tipe = tipe;
    }
  }
  //we will be needed to generate a lot of random numbers for this we will use this function
  function randomgen(min, max) {
    //the max value is exclusive
    return Math.floor(Math.random() * (max - min) + min);
  }
  //now, we may define some(I mean a lot of) moves
  //each move has its name, type and the damage is random
  quickattack = new Moves('Quick Attack', randomgen(15, 20), 'normal');
  thunderbolt = new Moves('Thunder Bolt', randomgen(25, 35), 'electric');
  irontail = new Moves('Iron Tail', randomgen(20, 30), 'steel');
  thunderpunch = new Moves('Thunder Punch', randomgen(20, 30), 'electric');
  flamethrower = new Moves('Flame Thrower', randomgen(25, 35), 'fire');
  dragonrage = new Moves('Dragon Rage', 40, 'dragon');
  wingattack = new Moves('Wing Attack', randomgen(20, 30), 'fly');
  skullbash = new Moves('Skull Bash', randomgen(10, 20), 'normal');
  hydropump = new Moves('Hydro Pump', randomgen(15, 25), 'water');
  bubblebeam = new Moves('Bubble Beam', randomgen(10, 20), 'water');
  balt = new Moves('balt', randomgen(20, 30), 'dark');
  vinewhip = new Moves('Vine Whip', randomgen(15, 25), 'grass');
  razorleaf = new Moves('Razor Leaf', randomgen(20, 30), 'grass');
  takedown = new Moves('Take Down', randomgen(25, 30), 'normal');
  solarbeam = new Moves('Solar Beam', randomgen(50, 80), 'grass');
  dragonclaw = new Moves('Dragon Claw', randomgen(25, 40), 'dragon');
  shadowclaw = new Moves('Shadow Claw', randomgen(25, 40), 'ghost');
  //new update will bring these moves to life
  hyperbeam = new Moves('Hyper Beam', randomgen(60, 120), 'normal');
  rockthrow = new Moves('Rock Throw', randomgen(25, 30), 'rock');
  earthquake = new Moves('Earthquake', randomgen(25, 40), 'ground');
  seismictoss = new Moves('Seismic Toss', randomgen(25, 35), 'fighting');
  shadowball = new Moves('Shadow Ball', randomgen(20, 30), 'ghost');
  psychic = new Moves('Psychic', randomgen(20, 30), 'psychic');
  icebeam = new Moves('Ice Beam', randomgen(25, 30), 'ice');
  darkpulse = new Moves('Dark Pulse', randomgen(25, 30), 'dark');
  psybeam = new Moves('Psybeam', randomgen(40, 50));
  //this miss is a special move which causes 0 damage
  //miss = new Moves('Missed', 0, null);
  //now we may define some(a lot of) pokemons
  //each pokemon will have a name, an array of moves, hp, cry, an array of weakness
  //a frontimage and a backimage
  //frontimage will be the opponent
  //backimage will be the player
  pikachu = new Pokemon('Pikachu', [quickattack, irontail, thunderpunch,
    thunderbolt
  ], 280, 'pika!', ['ground'], ['flying', 'steel', 'electric'], 'https://drive.google.com/uc?id=1cuhiMdO7PzV8PvXrX6zBDEnCXera18Bc', 'https://drive.google.com/uc?id=1dJY_5Gna5p4WqAxgdlPHCh-fwuuOCum9');
  blastoise = new Pokemon('Blastoise', [icebeam, balt, skullbash, hydropump], 330, 'blasssstoise!',
    ['electric', 'grass'], ['steel', 'fire', 'water', 'ice'], 'https://drive.google.com/uc?id=1EuIOP8WoDvCCO3_ciWR_kwv16h0vu4se', 'https://drive.google.com/uc?id=1qq4wx7lk0VcKejWQCmyoBdz_K1PGOJEb');
  charizard = new Pokemon('Charizard', [dragonclaw, shadowclaw, wingattack, flamethrower], 320, 'charrrr!',
    ['water', 'electric', 'rock'], ['fighting', 'bug', 'steel', 'fire', 'grass', 'fairy'], 'https://drive.google.com/uc?id=1fgBX8-nAhhKiXA5d3-gnu0ljgdghdlwx', 'https://drive.google.com/uc?id=1__gOclr2ybDVW4qMCmYOLdFMo5pFuFvP');
  venusaur = new Pokemon('Venusaur', [vinewhip, razorleaf, takedown, solarbeam], 340, 'venusaaaaur!',
    ['fire', 'flying', 'ice', 'psychic'], ['water', 'fighting', 'electric', 'grass', 'fairy'], 'https://drive.google.com/uc?id=1iAm7l8ZuzBQxnSOhciJjCuxFjsNf7fFO', 'https://drive.google.com/uc?id=18e_HEElcSBP2l6i0DXMnD2C75VAlvZEX');
  //these pokemons will be added soon;
  dragonite = new Pokemon('Dragonite', [thunderbolt, dragonclaw, wingattack, hyperbeam], 360, 'dragoooooniiiiite!!!!',
    ['rock', 'ice', 'dragon', 'fairy'], ['fighting', 'bug', 'fire', 'water', 'grass'], 'https://drive.google.com/uc?id=1w6mEw-w6nM_c3RZGOQ_pjEu0TBEIYSRt', 'https://drive.google.com/uc?id=119oIifbPEqb4WA0tA0A1wCMkIHGVDuAr');
  golem = new Pokemon('Golem', [takedown, rockthrow, earthquake, seismictoss], 360, 'Gooolllleeeem!!!!!!',
    ['grass', 'water', 'ice', 'fight', 'ground', 'steel'], ['normal', 'flying', 'poison', 'fire', 'electric'], 'https://drive.google.com/uc?id=1Tjqej-QJaMeDIcBgG8MMw_mO0jEkYsrR', 'https://drive.google.com/uc?id=14B7Uip0HoJyvQ78e_dHpeJkx_tdmca8w');
  gengar = new Pokemon('Gengar', [shadowball, psychic, shadowclaw, darkpulse], 280, 'Geeeeennnngaaaaar!!!',
    ['ground', 'psychic', 'ghost', 'dark'], ['poison', 'bug', 'grass', 'fairy'], 'https://drive.google.com/uc?id=17MQqE4QDsk9hJsupxqGbUekxmuRXPDO8', 'https://drive.google.com/uc?id=1d4HnBSU70fY3YBPxo7yA4STauPDHlw8W');
  gyarados = new Pokemon('Gyarados', [icebeam, hydropump, dragonrage, hyperbeam], 290, 'Gyaaaaarrrraaaaaadooooooss!!!!!!',
    ['electric', 'grass', 'rock'], ['fighting', 'fire', 'bug', 'steel', 'fire'], 'https://drive.google.com/uc?id=11XsEoKTDLK3mhx87wBg0G5S6AgM2bprg', 'https://drive.google.com/uc?id=1PzMe1DJNziaMheCgYRVqn-H6TgLuYeIs');
  //ultra special legendry most powerful pokemon

  //some events that will be fired when the attack buttons are pushed;
  attack1.click(function() {
    movecount = 0;
    //when the first button is clicked set the movecount to 0
    //call the function with redcorner as player, bluecorner as target and movecount as move
    attackCommentDefend(redcorner, bluecorner, movecount);
  });
  attack2.click(function() {
    movecount = 1;
    attackCommentDefend(redcorner, bluecorner, movecount);
  });
  attack3.click(function() {
    movecount = 2;
    attackCommentDefend(redcorner, bluecorner, movecount);
  });
  attack4.click(function() {
    movecount = 3;
    attackCommentDefend(redcorner, bluecorner, movecount);
  });
  //this function does commentry
  function showdialogue(attacker, moveused, comment, opponent) {
    //first we hide the attack options and show the commentbox
    attacks.css('display', 'none');
    commentBox.css('display', 'block');
    //then we put the attacker and the move it used and pass a comment about the attack
    yvWriter(commentBox, attacker.name + ' utilise ' + moveused + '~' + comment, 0, 50, true, function() {
      checkdeath(attacker, opponent);
    });
  }
  //if we click the fight button
  $('.fight').click(function() {
    //hide the options and commentBox and show the attacks
    options.css('display', 'none');
    attacks.fadeIn('slow');
    attacks.css('display', 'block');
    commentBox.css('display', 'none');
    redcorner.showoptions();
  })
  //if we click the change pokemon button
  $('.changepokemon').click(function() {
    options.css('display', 'none');
    yvWriter(commentBox, 'Changing Pokemon is not allowed in this battle.' + 'It will be definitely available in a later update', 0, 50, true, function() {
      commentBox.text('');
      options.css('display', 'block');
    }, 2000);
  })
  //if we click the run buttons
  $('.run').click(function() {
    options.css('display', 'none');
    yvWriter(commentBox, 'No running away in a pokemon battle', 0, 50, true, function() {
      options.css('display', 'block');
      commentBox.text('');
    }, 2000);
  })
  //do comment according to damage done by the attacker
  function docomment() {
    if(damagedone == 0) {
      comment = "C'est rater";
      return comment;
    } else if(damagedone == -5) {
      comment = "Mais a besoin de recharger";
      return comment;
    } else if(damagedone > 0 && damagedone <= 25) {
      comment = "Ce n'est pas trés efficace";
      return comment
    } else if(damagedone > 25 && damagedone <= 40) {
      comment = "AIE...!";
      return comment
    } else if(damagedone > 40 && damagedone <= 50) {
      comment = "C'est super efficace!!";
      return comment
    } else if(damagedone > 50) {
      comment = "C'est un coup critique!!!!!!";
      return comment
    }
  }
  //see if someone died
  function checkdeath(attacker, opponent) {
    if(opponent.hp <= 0 && messagewritten == false) {
      finished = true;
      yvWriter(commentBox, opponent.name + " n'est plus capable de se batrre ~ " + attacker.name + " Gagne le duel!", 0, 50, true, function() {
        messagewritten = true;
        if(opponent == redcorner) {
          $('#ending').css('background-image', "url('https://drive.google.com/uc?id=1sENIvHmAaEU_B8cIx3y_sLqw-5K1fUGb')");
        }
        $('#ending').css('zIndex', '10');
      }, 2000);
    }
  }
  //YvWriter adapted from Serena Pokemon Battle Code
  function yvWriter(target, message, idx, interval, clear, funcToExecute, dealy) {
    if(clear) $(target).html("");
    var intervalId = setInterval(function() {
      if(message[idx] === '~') {
        $(target).append("<br>");
        idx++;
      } else {
        $(target).append(message[idx++]);
      }
      if(idx >= message.length) {
        clearInterval(intervalId);
        setTimeout(funcToExecute, dealy);
      }
    }, interval);
  }
  //first attack then comment and then get attacked and comment
  function attackCommentDefend(me, opponent, move) {
    if(!finished) {
      //first we attack on the opponent using the move
      if(!finished) {
        me.attack(opponent, move);
      }
      if(opponent.hp < 0) {
        opponent.hp = 0;
      }
      //then we change the hp bar and number of opponent hp
      opponenthp.attr('value', opponent.hp);
      opponenthpnum.text(opponent.hp);
      //then we will decide which comment to do
      docomment();
      //then we will show that comment
      if(!finished) {
        showdialogue(me, me.moves[move].name, comment, opponent);
      }
      //after the comment is written we will check if the opponent died
    }
    // setTimeout(checkdeath(opponent, me), 8000);
    if(!finished) {
      //opponent move will be a random number
      move = randomgen(0, 4);
      //after attacking commenting now its opponent turn
      setTimeout(function() {
        //opponent attacks me
        if(!finished) {
          opponent.aiattack(me, move);
        }
        if(me.hp < 0) {
          me.hp = 0;
        }
        //we update our hp bar
        selfhp.attr('value', me.hp);
        selfhpnum.text(me.hp);
        //we decide a comment
        docomment();
        //and we show comment
        if(!finished) {
          showdialogue(opponent, opponent.moves[move].name, comment, me);
        }
      }, 6000);
    }
    //check if the player died
    // setTimeout(checkdeath(me, opponent), 8000);
    //now after all these
    //prompt the user to select his next move
    if(!finished) {
      setTimeout(function() {
        if(!finished) {
          yvWriter(commentBox, "Que doit " + me.name + " t'il faire maintenant ", 0, 50, true, function() {
            options.css('display', 'block');
          });
        }
      }, 12000);
    }
  }
  //the pokemons that we will be using
  var pokemons = [pikachu, blastoise, charizard, venusaur, dragonite, golem, gengar, gyarados];
  //a random pokemon from the pokemons list as redcorner or player
  var redcorner = pokemons[randomgen(0, pokemons.length)];
  // var redcorner = pikachu;
  //we will see which pokemon was selected for the player and we will remove it from the list
  let selectedpokemon = pokemons.indexOf(redcorner);
  pokemons.splice(selectedpokemon, 1);
  //now from the rest of the list we will select the opponnet pokemon or bluecorner
  var luckynumber = randomgen(1, 101);
  if(luckynumber > 90) {
    var bluecorner = mewtwo;
  } else {
    var bluecorner = pokemons[randomgen(0, pokemons.length)];
  }
  // var bluecorner = blastoise;
  //these are various things we want to set when the document loads
  //first set the images of our pokemon
  yvWriter("#greetings", "jeune dresseurs, Bienvenu dans mon  combat pokémon", 0, 100, false, function() {
    $("#greetings").html("jeune dresseurs, Bienvenu dans <b>mon  combat pokémon</b>")
  }, 0);
  setTimeout(function() {
    $("#start_button").css("animation", "fading 3s").css("opacity", "1");
  }, 5500);
  $("#start_button").click(function() {
    $("#start_menu").remove();
    $("#opponentLandSvg").css("animation", "slidingland 3s");
    $("#MyLandSvg").css("animation", "slidingpcl 3s");
    $("#oi").css("animation", "sliding 3s");
    $("#si").css("animation", "slidingpc 3s");
    options.css('display', 'none')
    yvWriter(commentBox, 'Ton pokemon est ' + redcorner.name + '~' + redcorner.cry, 0, 50, true, function() {
      yvWriter(commentBox, ' Le pokémon adversse est ' + bluecorner.name + '~' + bluecorner.cry, 0, 50, true, function() {
        yvWriter(commentBox, "Que " + redcorner.name + "'doit faire ", 0, 50, true, function() {
          options.css('display', 'block');
        }, 2000);
      }, 3000);
    }, 3000);
  })
  $('#oi').attr('src', bluecorner.frontimage);
  $('#si').attr('src', redcorner.backimage);
  //then update the hp bar of pokemons
  opponenthp.attr('value', bluecorner.hp);
  selfhp.attr('value', redcorner.hp);
  //the current hp at the start is the max hp so save its value and display
  //and also set the initial hp of pokemon in th hp num
  var opponentmaxhp = bluecorner.hp;
  $('#opponentmaxhp').text(opponentmaxhp);
  opponenthp.attr('max', bluecorner.hp);
  opponenthpnum.text(bluecorner.hp);
  var selfmaxhp = redcorner.hp;
  $('#selfmaxhp').text(selfmaxhp);
  selfhp.attr('max', redcorner.hp);
  selfhpnum.text(redcorner.hp);
  //set the opponent name and my name
  $('#opponentname').text(bluecorner.name);
  $('#selfname').text(redcorner.name);
});